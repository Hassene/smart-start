package Controllers;

import java.sql.*;
import java.util.ArrayList;

import connexion.DbLink;
import entities.User;

public class UserController {

	Connection con = DbLink.getInstance().getCnx();
	// ArrayList<User> users = new ArrayList<User>();

	public void displayUser() {
		try {
			Statement st = con.createStatement();
			String req = "select * from user";
			ResultSet result = st.executeQuery(req);
			while (result.next()) {
				String id = result.getString("id_user");
				String name = result.getString("username");
				String pass = result.getString("password");
				System.out.println("User numero: " + id + " username: " + name + " password: " + pass + "");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void addUser(User U) {

		try {
			Statement st = con.createStatement();
			String req = "insert into user (username,password) values ('" + U.getUsername() + "', '" + U.getPassword()
					+ "')";
			st.executeUpdate(req);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateUser(int id, String name, String pass) {

		try {
			Statement st = con.createStatement();
			String req = "update user set username = '" + name + "', password = '" + pass + "' where id_user = " + id;
			st.execute(req);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void deleteUser(int id) {

		try {
			Statement st = con.createStatement();
			String req = "delete from user where id_user = " + id;
			st.execute(req);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
