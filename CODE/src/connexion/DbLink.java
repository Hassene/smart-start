package connexion;

import java.sql.*;

public class DbLink {

	private static String URL = "jdbc:mysql://127.0.0.1:3306/smart_start";
	private static String USERNAME = "root";
	private static String PASSWORD = "";
	static Connection con;
	static DbLink instance;

	private DbLink() throws ClassNotFoundException {
		try {
			Class.forName("com.mysql.jdbc.Driver"); 
			con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			System.out.println("Connection succeeded");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static DbLink getInstance() {
		if (instance == null) {
			try {
				instance = new DbLink();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;

	}

	public static Connection getCnx() {
		return con;

	}

}
