package entities;

public class Admin extends User{
	
	private int id_admin;
	private String nom;
	private String prenom;
	
	public Admin(String username, String password, int id_admin, String nom, String prenom) {
		super(username, password);
		this.id_admin = id_admin;
		this.nom = nom;
		this.prenom = prenom;
	}

	public int getId_admin() {
		return id_admin;
	}

	public void setId_admin(int id_admin) {
		this.id_admin = id_admin;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Admin [id_admin=" + id_admin + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	

}
