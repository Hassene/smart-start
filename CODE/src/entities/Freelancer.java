package entities;

public class Freelancer extends User{

	private int id_freelancer;
	private String name;
	private int id_user;

	public Freelancer(String name, int id_user) {
		super();
		this.name = name;
		this.id_user = id_user;
	}

	public int getId_freelancer() {
		return id_freelancer;
	}

	public void setId_freelancer(int id_freelancer) {
		this.id_freelancer = id_freelancer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	@Override
	public String toString() {
		return "Freelancer [id_freelancer=" + id_freelancer + ", name=" + name + ", id_user=" + id_user + "]";
	}

}
