package entities;

public class FreelancerLigne {

	private int id_freelancerLigne;
	private int id_freelancer;
	private int id_skills;
	private int id_jobs;

	public FreelancerLigne(int id_freelancerLigne, int id_freelancer, int id_skills, int id_jobs) {
		super();
		this.id_freelancerLigne = id_freelancerLigne;
		this.id_freelancer = id_freelancer;
		this.id_skills = id_skills;
		this.id_jobs = id_jobs;
	}

	public int getId_freelancerLigne() {
		return id_freelancerLigne;
	}

	public void setId_freelancerLigne(int id_freelancerLigne) {
		this.id_freelancerLigne = id_freelancerLigne;
	}

	public int getId_freelancer() {
		return id_freelancer;
	}

	public void setId_freelancer(int id_freelancer) {
		this.id_freelancer = id_freelancer;
	}

	public int getId_skills() {
		return id_skills;
	}

	public void setId_skills(int id_skills) {
		this.id_skills = id_skills;
	}

	public int getId_jobs() {
		return id_jobs;
	}

	public void setId_jobs(int id_jobs) {
		this.id_jobs = id_jobs;
	}

	@Override
	public String toString() {
		return "FreelancerLigne [id_freelancerLigne=" + id_freelancerLigne + ", id_freelancer=" + id_freelancer
				+ ", id_skills=" + id_skills + ", id_jobs=" + id_jobs + "]";
	}

}
