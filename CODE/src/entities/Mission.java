package entities;

import java.util.Date;

public class Mission {

	private int id_mission;
	private String titre;
	private String niveau_difficulte;
	private Date date_deb;
	private Date date_fin;
	private double budget;
	private int id_freelancer;
	private int id_recruiter;
	private int id_categorie;

	public Mission(int id_mission, String titre, String niveau_difficulte, Date date_deb, Date date_fin, double budget,
			int id_freelancer, int id_recruiter, int id_categorie) {
		super();
		this.id_mission = id_mission;
		this.titre = titre;
		this.niveau_difficulte = niveau_difficulte;
		this.date_deb = date_deb;
		this.date_fin = date_fin;
		this.budget = budget;
		this.id_freelancer = id_freelancer;
		this.id_recruiter = id_recruiter;
		this.id_categorie = id_categorie;
	}

	public int getId_mission() {
		return id_mission;
	}

	public void setId_mission(int id_mission) {
		this.id_mission = id_mission;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getNiveau_difficulte() {
		return niveau_difficulte;
	}

	public void setNiveau_difficulte(String niveau_difficulte) {
		this.niveau_difficulte = niveau_difficulte;
	}

	public Date getDate_deb() {
		return date_deb;
	}

	public void setDate_deb(Date date_deb) {
		this.date_deb = date_deb;
	}

	public Date getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public int getId_freelancer() {
		return id_freelancer;
	}

	public void setId_freelancer(int id_freelancer) {
		this.id_freelancer = id_freelancer;
	}

	public int getId_recruiter() {
		return id_recruiter;
	}

	public void setId_recruiter(int id_recruiter) {
		this.id_recruiter = id_recruiter;
	}

	public int getId_categorie() {
		return id_categorie;
	}

	public void setId_categorie(int id_categorie) {
		this.id_categorie = id_categorie;
	}

	@Override
	public String toString() {
		return "Mission [id_mission=" + id_mission + ", titre=" + titre + ", niveau_difficulte=" + niveau_difficulte
				+ ", date_deb=" + date_deb + ", date_fin=" + date_fin + ", budget=" + budget + ", id_freelancer="
				+ id_freelancer + ", id_recruiter=" + id_recruiter + ", id_categorie=" + id_categorie + "]";
	}

}
