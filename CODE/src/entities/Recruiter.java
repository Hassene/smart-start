package entities;

public class Recruiter extends User {

	private int id_recruiter;
	private String nom;
	private String prenom;
	private String adress;
	private String e_mail;

	public Recruiter(String username, String password, int id_recruiter, String nom, String prenom, String adress,
			String e_mail) {
		super(username, password);
		this.id_recruiter = id_recruiter;
		this.nom = nom;
		this.prenom = prenom;
		this.adress = adress;
		this.e_mail = e_mail;
	}

	public int getId_recruiter() {
		return id_recruiter;
	}

	public void setId_recruiter(int id_recruiter) {
		this.id_recruiter = id_recruiter;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getE_mail() {
		return e_mail;
	}

	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}

	@Override
	public String toString() {
		return "Recruiter [id_recruiter=" + id_recruiter + ", nom=" + nom + ", prenom=" + prenom + ", adress=" + adress
				+ ", e_mail=" + e_mail + "]";
	}
	
	

}
