package entities;

public class SousMission {

	private int id_sousMission;
	private int id_mission;
	private int id_paiement;

	public SousMission(int id_sousMission, int id_mission, int id_paiement) {
		super();
		this.id_sousMission = id_sousMission;
		this.id_mission = id_mission;
		this.id_paiement = id_paiement;
	}

	public int getId_sousMission() {
		return id_sousMission;
	}

	public void setId_sousMission(int id_sousMission) {
		this.id_sousMission = id_sousMission;
	}

	public int getId_mission() {
		return id_mission;
	}

	public void setId_mission(int id_mission) {
		this.id_mission = id_mission;
	}

	public int getId_paiement() {
		return id_paiement;
	}

	public void setId_paiement(int id_paiement) {
		this.id_paiement = id_paiement;
	}

	@Override
	public String toString() {
		return "SousMission [id_sousMission=" + id_sousMission + ", id_mission=" + id_mission + ", id_paiement="
				+ id_paiement + "]";
	}

}
