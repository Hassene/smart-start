package entities;

public class Virement {

	private int id_virement;
	private int id_paiement;

	public Virement(int id_virement, int id_paiement) {
		super();
		this.id_virement = id_virement;
		this.id_paiement = id_paiement;
	}

	public int getId_virement() {
		return id_virement;
	}

	public void setId_virement(int id_virement) {
		this.id_virement = id_virement;
	}

	public int getId_paiement() {
		return id_paiement;
	}

	public void setId_paiement(int id_paiement) {
		this.id_paiement = id_paiement;
	}

	@Override
	public String toString() {
		return "Virement [id_virement=" + id_virement + ", id_paiement=" + id_paiement + "]";
	}

}
