/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import entities.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author mahmoudh
 */
public class LoginController implements Initializable {

    @FXML
    private JFXTextField SigninTXFusermame;
    @FXML
    private JFXPasswordField SigninTXFpass;
    @FXML
    private Hyperlink ForgotPassLabel;
    @FXML
    private JFXButton SigninButton;
    public  static User LoggedInUser ;

    /**
     * Initializes the controller class.
     */
    @Override
        public void initialize(URL url, ResourceBundle rb) {
        // TODO
        verifLogin();
         LoggedInUser= getLoggedInUser();
    }    
    
        private void verifLogin(){
            SigninButton.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                ServiceUser SU = new ServiceUser();
                if(SU.verifLogin(SigninTXFusermame.getText(), SigninTXFpass.getText()) == null){
                    System.out.println("error");
                }else{
                    try {   
                            Parent ProfilView = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
                            Scene ProfilScene = new Scene(ProfilView);
                            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                            window.setScene(ProfilScene);
                            window.show();
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
                }
                
            }
        });
    }  
    
        private void showSignUp(javafx.event.ActionEvent event) {
        try {
            Parent Signupview = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
            Scene Signupscene = new Scene(Signupview);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(Signupscene);
            window.show();
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
         public User getLoggedInUser(){
             
             ServiceUser Su = new ServiceUser();
             LoginController.LoggedInUser=(Su.verifLogin(SigninTXFusermame.getText(), SigninTXFpass.getText()));
             
             return LoggedInUser;
         }
        
}
       
       

