/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import entities.User;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author mahmoudh
 */
public class SignUpController implements Initializable {

    @FXML
    private JFXTextField SignupTXFnom;
    @FXML
    private JFXTextField SignupTXFprenom;
    @FXML
    private JFXDatePicker SignupDOB;
    @FXML
    private JFXTextField SignupTXFemail;
    @FXML
    private JFXTextField SignupTXFtel;
    @FXML
    private JFXRadioButton RadioFree;
    @FXML
    private JFXRadioButton RadioRecru;
    @FXML
    private JFXPasswordField SignupPFpass;
    @FXML
    private JFXPasswordField SignupPFconfPass;
    @FXML
    private Button adduserbutton;
    @FXML
    private JFXTextField SignupTXFusername;
    @FXML
    private ImageView Image;
    @FXML
    private Pane banner;
    
 
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        addUser();
    }    

   
    private void addUser(){
            adduserbutton.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                
                if(SignupPFpass.getText().equalsIgnoreCase(SignupPFconfPass.getText())){
                    if(RadioFree.isSelected()){
                        User u = new User(SignupTXFusername.getText(), SignupPFpass.getText(), SignupTXFnom.getText(), SignupTXFprenom.getText(), RadioFree.getText(), null, SignupTXFemail.getText(), null);
                        ServiceUser SU = new ServiceUser();
                        SU.addUser(u);
                    }else if(RadioRecru.isSelected()){
                        User u = new User(SignupTXFusername.getText(), SignupPFpass.getText(), SignupTXFnom.getText(), SignupTXFprenom.getText(), RadioRecru.getText(), null, SignupTXFemail.getText(), null);
                        ServiceUser SU = new ServiceUser();
                        SU.addUser(u);
                    }else{
                        System.out.println("select a type");
                    }
                
                }else{
                    System.out.println("error");
                }
                
                
            }
        });
        
}
}
