package Services;

import java.sql.*;
import java.util.ArrayList;

import connexion.DbLink;
import entities.User;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceUser {

    Connection con = DbLink.getInstance().getCnx();
    // ArrayList<User> users = new ArrayList<User>();

    public void displayUser() {
        try {
            Statement st = con.createStatement();
            String req = "select * from user";
            ResultSet result = st.executeQuery(req);
            while (result.next()) {
                String id = result.getString("id_user");
                String name = result.getString("username");
                String pass = result.getString("password");
                System.out.println("User numero: " + id + " username: " + name + " password: " + pass + "");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void addUser(User U) {

        try {
            
            String req = "insert into user (username,password,nom,prenom,type,status,email,adress) values (?,?,?,?,?,?,?,?)";
            PreparedStatement st = (PreparedStatement) con.prepareStatement(req);
            
            st.setString(1, U.getUsername());
            st.setString(2, U.getPassword());
            st.setString(3, U.getNom());
            st.setString(4, U.getPrenom());
            st.setString(5, U.getType());
            st.setString(6, U.getStatus());
            st.setString(7, U.getEmail());
            st.setString(8, U.getAdress());
            st.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void updateUser(int id, String name, String pass) {

        try {
            Statement st = con.createStatement();
            String req = "update user set username = '" + name + "', password = '" + pass + "' where id_user = " + id;
            st.execute(req);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void deleteUser(int id) {

        try {
            Statement st = con.createStatement();
            String req = "delete from user where id_user = " + id;
            st.execute(req);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public User verifLogin(String username, String pass) {
        User u = null;
        Statement st;
        try {
            st = con.createStatement();

            String req = "SELECT * "
                    + "from user "
                    + "where username like '" + username + "' and password like '" + pass+"'";
            //System.out.println(req);
            ResultSet result = st.executeQuery(req);

            while (result.next()) {
                u = new User();
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return u;
    }

}
