package entities;

public class CategorieMission {

	private int id_categorie;
	private String Designation;

	public CategorieMission(int id_categorie, String designation) {
		super();
		this.id_categorie = id_categorie;
		Designation = designation;
	}

	public int getId_categorie() {
		return id_categorie;
	}

	public void setId_categorie(int id_categorie) {
		this.id_categorie = id_categorie;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	@Override
	public String toString() {
		return "CategorieMission [id_categorie=" + id_categorie + ", Designation=" + Designation + "]";
	}

}
