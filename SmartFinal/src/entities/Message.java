package entities;

import java.util.Date;

public class Message {

	private int id_message;
	private String message;
	private Date date_envoi;
	private Date date_lecture;
	private int id_user1;
	private int id_user2;

	public Message(int id_message, String message, Date date_envoi, Date date_lecture, int id_user1, int id_user2) {
		super();
		this.id_message = id_message;
		this.message = message;
		this.date_envoi = date_envoi;
		this.date_lecture = date_lecture;
		this.id_user1 = id_user1;
		this.id_user2 = id_user2;
	}

	public int getId_message() {
		return id_message;
	}

	public void setId_message(int id_message) {
		this.id_message = id_message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate_envoi() {
		return date_envoi;
	}

	public void setDate_envoi(Date date_envoi) {
		this.date_envoi = date_envoi;
	}

	public Date getDate_lecture() {
		return date_lecture;
	}

	public void setDate_lecture(Date date_lecture) {
		this.date_lecture = date_lecture;
	}

	public int getId_user1() {
		return id_user1;
	}

	public void setId_user1(int id_user1) {
		this.id_user1 = id_user1;
	}

	public int getId_user2() {
		return id_user2;
	}

	public void setId_user2(int id_user2) {
		this.id_user2 = id_user2;
	}

	@Override
	public String toString() {
		return "Message [id_message=" + id_message + ", message=" + message + ", date_envoi=" + date_envoi
				+ ", date_lecture=" + date_lecture + ", id_user1=" + id_user1 + ", id_user2=" + id_user2 + "]";
	}

}
