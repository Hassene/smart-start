package entities;

import java.util.Date;

public class Paiement {

	private int id_paiement;
	private double amount;
	private Date date_paiement;
	private String currency;
	private int id_mission;

	public Paiement(int id_paiement, double amount, Date date_paiement, String currency, int id_mission) {
		super();
		this.id_paiement = id_paiement;
		this.amount = amount;
		this.date_paiement = date_paiement;
		this.currency = currency;
		this.id_mission = id_mission;
	}

	public int getId_paiement() {
		return id_paiement;
	}

	public void setId_paiement(int id_paiement) {
		this.id_paiement = id_paiement;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate_paiement() {
		return date_paiement;
	}

	public void setDate_paiement(Date date_paiement) {
		this.date_paiement = date_paiement;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getId_mission() {
		return id_mission;
	}

	public void setId_mission(int id_mission) {
		this.id_mission = id_mission;
	}

	@Override
	public String toString() {
		return "Paiement [id_paiement=" + id_paiement + ", amount=" + amount + ", date_paiement=" + date_paiement
				+ ", currency=" + currency + ", id_mission=" + id_mission + "]";
	}

}
