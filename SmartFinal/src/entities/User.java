package entities;

import java.util.Date;

public class User {

	private int id_user;
	private String username;
	private String password;
        private String nom;
        private String prenom;
        private String type;
        private String status;
        private String email;
        private String adress;
        private Date date_naissance;

    public User(String username, String password, String nom, String prenom, String type, String status, String email, String adress) {
        this.username = username;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.type = type;
        this.status = status;
        this.email = email;
        this.adress = adress;
        //this.date_naissance = date_naissance;
    }
        
    public User() {
		// TODO Auto-generated constructor stub
	}

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Date getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(Date date_naissance) {
        this.date_naissance = date_naissance;
    }

    @Override
    public String toString() {
        return "User{" + "id_user=" + id_user + ", username=" + username + ", password=" + password + ", nom=" + nom + ", prenom=" + prenom + ", type=" + type + ", status=" + status + ", email=" + email + ", adress=" + adress + ", date_naissance=" + date_naissance + '}';
    }

	

}
