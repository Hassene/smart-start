/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import entities.Mission;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.DbLink;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class AffichagemissionfreeController implements Initializable {

    @FXML
    private Pane banner;
    @FXML
    private Label label;
    @FXML
    private JFXButton Retour;
    @FXML
    private TableView<Mission> Table;
    @FXML
    private TableColumn<Mission, String> Titrefx;
    @FXML
    private TableColumn<Mission, String> descriptionfx;
    @FXML
    private ListView<Mission> listvie;
      public ObservableList<Mission> listmission3 = FXCollections.observableArrayList();
int idclick;
      public ObservableList<Mission> listmission4 = FXCollections.observableArrayList();


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setLabel(label);
        afficherliste();
        Exitpageprincipale();
        selectionnermission();
    }
    public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }
     public void afficherliste(){
         Connection con = DbLink.getInstance().getCnx();
        try {
            Statement st = con.createStatement();
            String req = "select * from mission";
            ResultSet result = st.executeQuery(req);
            while (result.next()) {
         listmission3.add(new Mission(result.getInt("id_mission") ,result.getString("titre") ,result.getString("description") ,result.getString("details") ,result.getString("Skills") ,result.getString("Visibilite") ,result.getString("nombre") ,result.getString("moyendepayment") ,result.getString("niveaudexperience") ,result.getString("duree") ));   
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Titrefx.setCellValueFactory(new PropertyValueFactory<>("titre"));
    descriptionfx.setCellValueFactory(new PropertyValueFactory<>("description"));
        
    Table.setItems(listmission3);
  
    }
    
     private void Exitpageprincipale(){
       Retour.setOnMouseClicked((MouseEvent event) -> {
           try {
                Parent titreView = FXMLLoader.load(getClass().getResource("DashboardFreelancer.fxml"));
               Scene DashScene = new Scene(titreView);
                    Stage dashrec = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    dashrec.setScene(DashScene);
                    dashrec.show();
           } catch (IOException ex) {
Logger.getLogger(DashboardFreelancerController.class.getName()).log(Level.SEVERE, null, ex);           }
                       
       });        
}  
    
  public void selectionnermission(){
       Table.setOnMouseClicked(s -> {
         Connection con = DbLink.getInstance().getCnx();
        try {
            Statement st = con.createStatement();         
   Mission Missionchoisi = Table.getSelectionModel().getSelectedItem();
   idclick = Missionchoisi.getId_mission();
            System.out.println(idclick);
String req1 = "select * from mission where id_mission = " + idclick;
System.out.println(req1);
            ResultSet result = st.executeQuery(req1);
                       while (result.next()) {

         listmission4.add(new Mission(result.getString("titre") ,result.getString("description") ,result.getString("details") ,result.getString("Skills") ,result.getString("Visibilite") ,result.getString("nombre") ,result.getString("moyendepayment") ,result.getString("niveaudexperience") ,result.getString("duree") ));  
                       }   
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                  
    listvie.setItems(listmission4);
    });} 
     
     
     
     
     
    
    
}
