/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import entities.Mission;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class BudgetController implements Initializable {

    @FXML
    private Pane banner;
    @FXML
    private Label label;
    private String titre; 
         private String details;
         private String skills;
         private String description;
         private String Visibilite;
         private String nombrefreelancers;
          private String moyendepayment;
           private String montantapayer;
       private String niveaudexperience;
       private String duree;

    @FXML
          private JFXTextField montant;

    @FXML
    private JFXButton retourvisibilite;
    @FXML
    private JFXButton parheure;
    @FXML
    private JFXButton partotalite;
    @FXML
    private JFXButton Nextavis;
    @FXML
    private JFXButton Debutant;
    @FXML
    private JFXButton Intermediaire;
    @FXML
    private JFXButton Expert;
    @FXML
    private JFXButton Plusque6mois;
    @FXML
    private JFXButton troisasixmois;
    @FXML
    private JFXButton unatroismois;

  

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVisibilite(String Visibilite) {
        this.Visibilite = Visibilite;
    }

    public void setNombrefreelancers(String nombrefreelancers) {
        this.nombrefreelancers = nombrefreelancers;
    }
         

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setLabel(label);
        retourvisiblite();
        handlemoyenpayment();
        handleniveaudexperience();
        handleduree();
        movetoAvis();
    }    
    public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }
    
    private void retourvisiblite(){
       retourvisibilite.setOnMouseClicked((MouseEvent event) -> {
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Visibilite.fxml"));
                  Parent root = Loader.load();
                  VisibiliteController vi = Loader.getController();
                  vi.setDescription(description);
                  vi.setTitre(titre);
                  vi.setDetails(details);
                  vi.setSkills(skills);
                
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene visibiliteScene = new Scene(root);
                    Stage visibilite = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    visibilite.setScene(visibiliteScene);
                    visibilite.show();
           } catch (IOException ex) {
               Logger.getLogger(DetailsController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
       });        
}
     private String handlemoyenpayment(){
       parheure.setOnMouseClicked((MouseEvent event) -> {
               moyendepayment= "Par heure";
          parheure.setOnAction((ActionEvent e) -> {
   parheure.getStyleClass().add("addBobOk");
   partotalite.getStyleClass().remove("addBobOk");
   
});   
          
       });
       partotalite.setOnMouseClicked((MouseEvent event) -> {
            moyendepayment="Par totalite";
            montantapayer = montant.getText();
           partotalite.setOnAction((ActionEvent e) -> {       
    partotalite.getStyleClass().add("addBobOk");
   parheure.getStyleClass().remove("addBobOk");
   
});   
                       
       });
                 
     return moyendepayment;        
     }
    
    
     
     
      private void senddatabase(){
       
       Nextavis.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
        
               Mission M = new Mission(titre,description,details,skills,Visibilite,nombrefreelancers,moyendepayment,niveaudexperience,duree);
                System.out.println(M);
          // ServiceUser SU = new ServiceUser();
                       // SU.addmission(M);
           
            }
            
            });   
}
      
      
      
      
  private String handleniveaudexperience(){
       Debutant.setOnMouseClicked((MouseEvent event) -> {
               niveaudexperience= "Debutant";
          Debutant.setOnAction((ActionEvent e) -> {
   Debutant.getStyleClass().add("addBobOk");
   Intermediaire.getStyleClass().remove("addBobOk");
      Expert.getStyleClass().remove("addBobOk");

   
});   
          
       });
       Intermediaire.setOnMouseClicked((MouseEvent event) -> {
            niveaudexperience="Intermediaire";
           Intermediaire.setOnAction((ActionEvent e) -> {       
    Intermediaire.getStyleClass().add("addBobOk");
   Debutant.getStyleClass().remove("addBobOk");
   Expert.getStyleClass().remove("addBobOk");
   
});   
                       
       });
       
        Expert.setOnMouseClicked((MouseEvent event) -> {
            niveaudexperience="Expert";
           Expert.setOnAction((ActionEvent e) -> {       
    Expert.getStyleClass().add("addBobOk");
   Debutant.getStyleClass().remove("addBobOk");
   Intermediaire.getStyleClass().remove("addBobOk");
   
});   
                       
       });
       
       
       
                 
     return niveaudexperience;        
     }    
      
   private String handleduree(){
       Plusque6mois.setOnMouseClicked((MouseEvent event) -> {
               duree= "Plus que 6 mois";
          Plusque6mois.setOnAction((ActionEvent e) -> {
   Plusque6mois.getStyleClass().add("addBobOk");
   troisasixmois.getStyleClass().remove("addBobOk");
   unatroismois.getStyleClass().remove("addBobOk");
});   
          
       });
       troisasixmois.setOnMouseClicked((MouseEvent event) -> {
            duree="Trois à Six mois";
           troisasixmois.setOnAction((ActionEvent e) -> {       
    troisasixmois.getStyleClass().add("addBobOk");
   Plusque6mois.getStyleClass().remove("addBobOk");
    unatroismois.getStyleClass().remove("addBobOk");
});   
                       
       });
          unatroismois.setOnMouseClicked((MouseEvent event) -> {
            duree="Trois à Six mois";
           unatroismois.setOnAction((ActionEvent e) -> {       
    unatroismois.getStyleClass().add("addBobOk");
   Plusque6mois.getStyleClass().remove("addBobOk");
    troisasixmois.getStyleClass().remove("addBobOk");
});   
                       
       });
                 
     return duree;        
     }   
 
        private void movetoAvis(){
       Nextavis.setOnMouseClicked((MouseEvent event) -> {
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Recap.fxml"));
                  Parent root = Loader.load();
                  RecapController rec = Loader.getController();
                  rec.setDescription(description);
                  rec.setTitre1(titre);
                  rec.setDetails(details);
                  rec.setSkills(skills);
                rec.setVisibilite(Visibilite);
                rec.setNombrefreelancers(nombrefreelancers);
                rec.setMoyendepayment(moyendepayment);
                rec.setNiveaudexperience(niveaudexperience);
                rec.setDuree(duree);
                
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene recapScene = new Scene(root);
                    Stage Recap = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Recap.setScene(recapScene);
                    Recap.show();
           } catch (IOException ex) {
               Logger.getLogger(BudgetController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
       });       
            }     
    
}
