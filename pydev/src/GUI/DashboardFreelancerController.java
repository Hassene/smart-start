/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class DashboardFreelancerController implements Initializable {

    @FXML
    private Pane banner;
    @FXML
    private Label label;
    @FXML
    private JFXButton afficherliste;
    


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      setLabel(label);
      pageaffichermissions();
     
    }    
public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }

private void pageaffichermissions(){
       afficherliste.setOnMouseClicked((MouseEvent event) -> {
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("affichagemissionfree.fxml"));
               Parent root = Loader.load();
               AffichagemissionfreeController lc = Loader.getController();
              // Parent descriptionView = FXMLLoader.load(getClass().getResource("Description.fxml"));
               Scene descriptionScene = new Scene(root);
                    Stage desc = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    desc.setScene(descriptionScene);
                    desc.show();
           } catch (IOException ex) {
               Logger.getLogger(DashboardFreelancerController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
                 });        
}


}
