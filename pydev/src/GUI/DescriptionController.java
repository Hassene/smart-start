/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import entities.Mission;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class DescriptionController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private JFXButton suivantdes;
    @FXML
    private JFXButton retourtitre;
    @FXML
    private JFXTextArea descriptionmission;
    private String titre;
    private String description;
    @FXML
    private Label erreur;

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setLabel(label);
        //savejobtitle();
        suivant();
        retourtitre();
        // TODO
    }

    public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }

    private void suivant() {

        suivantdes.setOnMouseClicked((MouseEvent event) -> {
            if (descriptionmission.getText().length() == 0) {
                erreur.setText("Merci d'Entrer la description de la mission !");

            } else if (descriptionmission.getText().length() != 0){
                try {
                    FXMLLoader Loader = new FXMLLoader(getClass().getResource("Details.fxml"));
                    Parent root = Loader.load();
                    DetailsController dt = Loader.getController();
                    dt.setDescription(descriptionmission.getText());
                    dt.setTitre(titre);
                    //Parent descriptionView = FXMLLoader.load(getClass().getResource("Details.fxml"));
                    Scene descriptionScene = new Scene(root);
                    Stage details = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    details.setScene(descriptionScene);
                    details.show();
                } catch (IOException ex) {
                    Logger.getLogger(DescriptionController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
    }

    private void retourtitre() {
        retourtitre.setOnMouseClicked((MouseEvent event) -> {
            try {
                Parent titreView = FXMLLoader.load(getClass().getResource("Titrerecruiter.fxml"));
                Scene titreScene = new Scene(titreView);
                Stage details = (Stage) ((Node) event.getSource()).getScene().getWindow();
                details.setScene(titreScene);
                details.show();
            } catch (IOException ex) {
                Logger.getLogger(DescriptionController.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
    }

    private void savejobtitle() {

        suivantdes.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {

                //   Mission  M = new Mission(titre,"Moyen",descriptionmission.getText());
                // System.out.println(M);
                //ServiceUser SU = new ServiceUser();
                //  SU.addmission(M);
            }

        });

    }

}
