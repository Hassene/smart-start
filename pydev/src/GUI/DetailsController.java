/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class DetailsController implements Initializable {

    private AnchorPane Details;
    @FXML
    private Pane banner;
    @FXML
    private Label label;
    @FXML
    private JFXButton retourdes;
    @FXML
    private JFXButton Nexttoexpertise;
    private String description;
    @FXML
    private JFXButton Onetime;
    @FXML
    private JFXButton Ongoing;
    @FXML
    private JFXButton Notsure;
    @FXML
    private Label erreur;

    public void setDescription(String description) {
        this.description = description;
    }
     private String titre;
     private String details;

    public void setTitre(String titre) {
        this.titre = titre;
    }
     
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        setLabel(label);
        retourdes();
        handlebutton();
       
        movetoexpertise();
    }    
    
    public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }
    private void retourdes(){
       retourdes.setOnMouseClicked((MouseEvent event) -> {
           
               try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Description.fxml"));
                  Parent root = Loader.load();
                  DescriptionController des = Loader.getController();
                  des.setTitre(titre);
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene descriptionScene = new Scene(root);
                    Stage details = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    details.setScene(descriptionScene);
                    details.show();
           } catch (IOException ex) {
               Logger.getLogger(DescriptionController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
       });        
}

 private void movetoexpertise(){
       Nexttoexpertise.setOnMouseClicked((MouseEvent event) -> {
           if (details == null) {
           erreur.setText("veuillez selectionner une option  !");
           }
           else if (details != null){  
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Expertise.fxml"));
                  Parent root = Loader.load();
                  ExpertiseController ex = Loader.getController();
                  ex.setDescription(description);
                  ex.setTitre(titre);
                  ex.setDetails(details);
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene ExpertiseScene = new Scene(root);
                    Stage Expertise = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Expertise.setScene(ExpertiseScene);
                    Expertise.show();
           } catch (IOException ex) {
               Logger.getLogger(DetailsController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
           }  });        
}

  private String handlebutton(){
       Onetime.setOnMouseClicked((MouseEvent event) -> {
               details= "One Time Project";
          Onetime.setOnAction((ActionEvent e) -> {
   Onetime.getStyleClass().add("addBobOk");
   Ongoing.getStyleClass().remove("addBobOk");
   Notsure.getStyleClass().remove("addBobOk");
});   
               
       });
       Ongoing.setOnMouseClicked((MouseEvent event) -> {
           
               details= "On Going Project"; 
           Ongoing.setOnAction((ActionEvent e) -> {
    
    Ongoing.getStyleClass().add("addBobOk");
   Onetime.getStyleClass().remove("addBobOk");
   Notsure.getStyleClass().remove("addBobOk"); 
});   
                       
       });
      Notsure.setOnMouseClicked((MouseEvent event) -> {
           
               details= "Not sure"; 
           Notsure.setOnAction((ActionEvent e) -> {
    Notsure.getStyleClass().add("addBobOk");
   Onetime.getStyleClass().remove("addBobOk");
   Ongoing.getStyleClass().remove("addBobOk");
});   
                       
       });
       
       
       return details;
}

   
 
 

 

}