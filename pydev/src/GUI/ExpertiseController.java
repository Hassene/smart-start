/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import com.jfoenix.controls.JFXButton;
import entities.Skills;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.DbLink;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class ExpertiseController implements Initializable {

    @FXML
    private Pane banner;
    @FXML
    private Label label;
    @FXML
    private JFXButton Retourdetails;
private String description;
    private String titre; 
         private String details;
         private String skills;
    @FXML
    private JFXButton Nextvisibilite;
    @FXML
    public TableView<Skills> Table;
    @FXML
    private TableColumn<Skills, String> col_id;
    @FXML
    private TableColumn<Skills, String> col_competence;
  
 public ObservableList<Skills> oblist = FXCollections.observableArrayList();

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO*
            setLabel(label);
        retourdetails();
        affichetableau();
        saveSkill();
        movetoVisibilite();
        
    }    

    
    public void affichetableau(){
         Connection con = DbLink.getInstance().getCnx();
        try {
            Statement st = con.createStatement();
            String req = "select * from skills";
            ResultSet result = st.executeQuery(req);
            while (result.next()) {
              oblist.add(new Skills(result.getInt("id_skills") ,result.getString("titre") ));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    col_id.setCellValueFactory(new PropertyValueFactory<>("id_skills"));
    col_competence.setCellValueFactory(new PropertyValueFactory<>("designation"));
    
  Table.setItems(oblist);
  
    }
    
    
    
    public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }
    private void retourdetails(){
       Retourdetails.setOnMouseClicked((MouseEvent event) -> {
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Details.fxml"));
                  Parent root = Loader.load();
                  DetailsController det = Loader.getController();
                  det.setDescription(description);
                  det.setTitre(titre);
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene detailsScene = new Scene(root);
                    Stage details = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    details.setScene(detailsScene);
                    details.show();
           } catch (IOException ex) {
               Logger.getLogger(DetailsController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
       });        
}
 
    
    
    private void saveSkill(){
       
       Nextvisibilite.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
          Skills skillchoisi = Table.getSelectionModel().getSelectedItem();
         //Skills skillchoisi = Table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
   skills = skillchoisi.getDesignation();
           
            }
            
            });
 
    
}
    
     private void movetoVisibilite(){
       Nextvisibilite.setOnMouseClicked((MouseEvent event) -> {
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Visibilite.fxml"));
                  Parent root = Loader.load();
                  VisibiliteController vi = Loader.getController();
                  vi.setDescription(description);
                  vi.setTitre(titre);
                  vi.setDetails(details);
                  vi.setSkills(skills);
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene VisibiliteScene = new Scene(root);
                    Stage Expertise = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Expertise.setScene(VisibiliteScene);
                    Expertise.show();
           } catch (IOException ex) {
               Logger.getLogger(DetailsController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
       });        
}
    
}
