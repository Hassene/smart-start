/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author mahmoudh
 */
public class ForgotPasswordController implements Initializable {

    @FXML
    private Pane banner;
    @FXML
    private JFXButton ResetPassButton;
    @FXML
    private ImageView Image;
    @FXML
    private Hyperlink SignInLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          showSignIn();
    }    
    
       private void showSignIn(){
            SignInLabel.setOnAction((ActionEvent event) -> {
                try {
                    Parent LoginView = FXMLLoader.load(getClass().getResource("Login.fxml"));
                    Scene LoginScene = new Scene(LoginView);
                    Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    window.setScene(LoginScene);
                    window.show();
                } catch (IOException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }   });
    }
    
    
    
}
