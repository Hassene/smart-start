/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author mahmoudh
 */
public class HomeController implements Initializable {
        @FXML 
        Button SignIn;
        @FXML
        Button Signup;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        showLogin();
        showSignUp();
    }    
    
    private void showLogin(){
            SignIn.setOnAction((ActionEvent event) -> {
                try {
                    Parent ProfilView = FXMLLoader.load(getClass().getResource("Login.fxml"));
                    Scene ProfilScene = new Scene(ProfilView);
                    Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    window.setScene(ProfilScene);
                    window.show();
                } catch (IOException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }   });
    }
     
    private void showSignUp(){
            Signup.setOnAction((ActionEvent event) -> {
                try {
                    Parent ProfilView = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
                    Scene ProfilScene = new Scene(ProfilView);
                    Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    window.setScene(ProfilScene);
                    window.show();
                } catch (IOException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }   });
    }
    
    //ShowCommentCaMarche
}
                    