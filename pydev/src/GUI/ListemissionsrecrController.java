/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import entities.Mission;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.DbLink;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class ListemissionsrecrController implements Initializable {

    @FXML
    private Pane banner;
    @FXML
    private Label label;
    
 public ObservableList<Mission> listmission = FXCollections.observableArrayList();
  public ObservableList<Mission> listmission2 = FXCollections.observableArrayList();
   
 
 @FXML
    private JFXButton Retour;
    @FXML
    private TableView<Mission> Table;
    
    private TableColumn<Mission, String> titremissionfx;
    @FXML
    private ListView<Mission> listvie;
    public int idclick;
    @FXML
    private TableColumn<Mission, String> Titrefx;
    @FXML
    private TableColumn<Mission, String> descriptionfx;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setLabel(label);
        afficherliste();
        Exitpageprincipale();
        selectionnermission();
    }  
    
     public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }
    public void afficherliste(){
         Connection con = DbLink.getInstance().getCnx();
        try {
            Statement st = con.createStatement();
            String req = "select * from mission";
            ResultSet result = st.executeQuery(req);
            while (result.next()) {
         listmission.add(new Mission(result.getInt("id_mission") ,result.getString("titre") ,result.getString("description") ,result.getString("details") ,result.getString("Skills") ,result.getString("Visibilite") ,result.getString("nombre") ,result.getString("moyendepayment") ,result.getString("niveaudexperience") ,result.getString("duree") ));


            
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Titrefx.setCellValueFactory(new PropertyValueFactory<>("titre"));
    descriptionfx.setCellValueFactory(new PropertyValueFactory<>("description"));
        
    Table.setItems(listmission);
  
    }
    
    public void selectionnermission(){
       Table.setOnMouseClicked(s -> {
         Connection con = DbLink.getInstance().getCnx();
        try {
            Statement st = con.createStatement();         
   Mission Missionchoisi = Table.getSelectionModel().getSelectedItem();
   idclick = Missionchoisi.getId_mission();
            System.out.println(idclick);
String req1 = "select * from mission where id_mission = " + idclick;
System.out.println(req1);
            ResultSet result = st.executeQuery(req1);
                       while (result.next()) {

         listmission2.add(new Mission(result.getInt("id_mission") ,result.getString("titre") ,result.getString("description") ,result.getString("details") ,result.getString("Skills") ,result.getString("Visibilite") ,result.getString("nombre") ,result.getString("moyendepayment") ,result.getString("niveaudexperience") ,result.getString("duree") ));  
                       }   
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }          
           
    listvie.setItems(listmission2);
    });}
    
      private void Exitpageprincipale(){
       Retour.setOnMouseClicked((MouseEvent event) -> {
           try {
                Parent titreView = FXMLLoader.load(getClass().getResource("DashboardRecruiter.fxml"));
               Scene DashScene = new Scene(titreView);
                    Stage dashrec = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    dashrec.setScene(DashScene);
                    dashrec.show();
           } catch (IOException ex) {
Logger.getLogger(DashboardRecruiterController.class.getName()).log(Level.SEVERE, null, ex);           }
                       
       });        
} 
    
    
    
    
    
}
