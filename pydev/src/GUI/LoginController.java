/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import entities.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author mahmoudh
 */
public class LoginController implements Initializable {

    @FXML
    private JFXTextField SigninTXFusermame;
    @FXML
    private JFXPasswordField SigninTXFpass;
    @FXML
    private Hyperlink ForgotPassLabel;
    @FXML
    private JFXButton SigninButton;
    public  static User LoggedInUser ;
    @FXML
    private JFXButton SignUpButton;

    /**
     * Initializes the controller class.
     */
    @Override
        public void initialize(URL url, ResourceBundle rb) {
        verifLogin();
        showForgotPassword ();
        showSignUp();
       
    }    
    
        private void verifLogin(){
            SigninButton.setOnAction((javafx.event.ActionEvent event) -> {
                ServiceUser SU = new ServiceUser();
                User u =SU.verifLogin(SigninTXFusermame.getText(), SigninTXFpass.getText());
                Statics.currentUser=u;
                System.out.println(u);
                if(u==null){
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Verifier Vos Informations");
                    alert.setHeaderText("Verifier Vos Information");
                    alert.setContentText("Ooops, Une erreur a eu lieu!");
                    alert.showAndWait();
                }else if (u.getType().equalsIgnoreCase("Freelancer")){
                  try {
                        Parent ProfilView = FXMLLoader.load(getClass().getResource("DashboardFreelancer.fxml"));
                        Scene ProfilScene = new Scene(ProfilView);
                        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        window.setScene(ProfilScene);
                        window.show();
                    } catch (IOException ex) {
                        Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if (u.getType().equalsIgnoreCase("Recruiteur")){
                  try {
                        Parent ProfilView = FXMLLoader.load(getClass().getResource("DashboardRecruiter.fxml"));
                        Scene ProfilScene = new Scene(ProfilView);
                        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        window.setScene(ProfilScene);
                        window.show();
                    } catch (IOException ex) {
                        Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else {
                     try {
                        Parent ProfilView = FXMLLoader.load(getClass().getResource("DashboardAdmin.fxml"));
                        Scene ProfilScene = new Scene(ProfilView);
                        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        window.setScene(ProfilScene);
                        window.show();
                    } catch (IOException ex) {
                        Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            });
    }  
    
        private void showSignUp() {
            SignUpButton.setOnAction((ActionEvent event) -> {
                try {
                    Parent Signupview = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
                    Scene Signupscene = new Scene(Signupview);
                    Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    window.setScene(Signupscene);
                    window.show();
                } catch (IOException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }   });
         
    }
        
        private void showForgotPassword (){
             ForgotPassLabel.setOnAction((ActionEvent event) -> {
            try {
                Parent ForgotPasswordview = FXMLLoader.load(getClass().getResource("ForgotPassword.fxml"));
                Scene  ForgotPasswodScene = new Scene(ForgotPasswordview);
                Stage  window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(ForgotPasswodScene);
                window.show();
            } catch (IOException ex){
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE,null,ex);
                } 
        });
}
         
}
       

