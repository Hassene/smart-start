/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import com.jfoenix.controls.JFXButton;
import entities.Mission;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class RecapController implements Initializable {
private String titre1; 
         private String details;
         private String skills;
         private String description;
         private String Visibilite;
         private String nombrefreelancers;
         private String moyendepayment;
         private String montantapayer;
         private String niveaudexperience;
         private String duree;
         
  
         
    @FXML
    private TableColumn<Mission, String> Visibilitefx;
    @FXML
    private TableColumn<Mission, String> Nombrefx;
    @FXML
    private TableColumn<Mission, String> Moyendepaymentfx;
    @FXML
    private TableColumn<Mission, String> Niveaudexperiencefx;
    @FXML
    private TableColumn<Mission, String> dureefx;
    @FXML
    private JFXButton retourbudget;
    @FXML
    private TableView<Mission> table;
    @FXML
    private TableColumn<Mission, String> id;
    @FXML
    private TableColumn<Mission, String> titrefx;
    @FXML
    private TableColumn<Mission, String> descriptionfx;
    @FXML
    private TableColumn<Mission, String> detailsfx;
    @FXML
    private TableColumn<Mission, String> skillsfx;
    @FXML
    private JFXButton envoyer;
    @FXML
    private JFXButton afficher;

    public void setTitre1(String titre1) {
        this.titre1 = titre1;
    }

  

    public void setDetails(String details) {
        this.details = details;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVisibilite(String Visibilite) {
        this.Visibilite = Visibilite;
    }

    public void setNombrefreelancers(String nombrefreelancers) {
        this.nombrefreelancers = nombrefreelancers;
    }

    public void setMoyendepayment(String moyendepayment) {
        this.moyendepayment = moyendepayment;
    }

    public void setMontantapayer(String montantapayer) {
        this.montantapayer = montantapayer;
    }

    public void setNiveaudexperience(String niveaudexperience) {
        this.niveaudexperience = niveaudexperience;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }
       
    @FXML
    private Pane banner;
    @FXML
    private Label label;

    /**
     * Initializes the controller class.
     */
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setLabel(label);
       affichetableau();
       envoyerdatabase();
        retourbudget();
        Suivantpageprincipale();
    }    
 public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
      
    }

 
 
  public ObservableList<Mission> oblist2 = FXCollections.observableArrayList();

 
    
    
     
    private void retourbudget(){
       retourbudget.setOnMouseClicked((MouseEvent event) -> {
           try {
                FXMLLoader Loader = new FXMLLoader(getClass().getResource("Budget.fxml"));
                  Parent root = Loader.load();
                  BudgetController bc = Loader.getController();
                  bc.setDescription(description);
                  bc.setTitre(titre1);
                  bc.setDetails(details);
                  bc.setSkills(skills);
                  bc.setVisibilite(Visibilite);
                  bc.setNombrefreelancers(nombrefreelancers);
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene budgetScene = new Scene(root);
                    Stage Expertise = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Expertise.setScene(budgetScene);
                    Expertise.show();
           } catch (IOException ex) {
               Logger.getLogger(RecapController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
       });        
}
    
    
      private void affichetableau(){
          
       afficher.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
               Mission M = new Mission(titre1,description,details,skills,Visibilite,nombrefreelancers,moyendepayment,niveaudexperience,duree);
                System.out.println(M);
                 oblist2.add(M);

   titrefx.setCellValueFactory(new PropertyValueFactory<>("titre"));
   descriptionfx.setCellValueFactory(new PropertyValueFactory<>("description"));
   detailsfx.setCellValueFactory(new PropertyValueFactory<>("details"));
   skillsfx.setCellValueFactory(new PropertyValueFactory<>("Skills"));
   Visibilitefx.setCellValueFactory(new PropertyValueFactory<>("Visibilite"));
   Nombrefx.setCellValueFactory(new PropertyValueFactory<>("nombrefreelancers"));
   Moyendepaymentfx.setCellValueFactory(new PropertyValueFactory<>("moyendepayment"));
   Niveaudexperiencefx.setCellValueFactory(new PropertyValueFactory<>("niveaudexperience"));
    dureefx.setCellValueFactory(new PropertyValueFactory<>("duree"));

    table.setItems(oblist2);
          // ServiceUser SU = new ServiceUser();
                       // SU.addmission(M);
           
            }
            
            });   
}
      
private void envoyerdatabase(){
envoyer.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
        Mission M = new Mission(titre1,description,details,skills,Visibilite,nombrefreelancers,moyendepayment,niveaudexperience,duree);
                System.out.println(M);
         ServiceUser SU = new ServiceUser();
                    SU.addmission(M);
           
            }
            
            }); 


}


  private void Suivantpageprincipale(){
       envoyer.setOnMouseClicked((MouseEvent event) -> {
           try {
                Parent titreView = FXMLLoader.load(getClass().getResource("DashboardRecruiter.fxml"));
               Scene DashScene = new Scene(titreView);
                    Stage dashrec = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    dashrec.setScene(DashScene);
                    dashrec.show();
           } catch (IOException ex) {
Logger.getLogger(DashboardRecruiterController.class.getName()).log(Level.SEVERE, null, ex);           }
                       
       });        
} 





}     
    
   
    
