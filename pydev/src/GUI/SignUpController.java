/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.Send_mail;
import Services.ServiceUser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import entities.Mail;
import entities.User;
import java.io.IOException;
import java.net.URL;
import java.time.Month;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import Services.Send_mail;


public class SignUpController implements Initializable {

    @FXML
    private JFXTextField SignupTXFnom;
    @FXML
    private JFXTextField SignupTXFprenom;
    @FXML
    private JFXTextField SignupTXFemail;
    @FXML
    private JFXRadioButton RadioFree;
    @FXML
    private JFXRadioButton RadioRecru;
    @FXML
    private JFXPasswordField SignupPFpass;
    @FXML
    private JFXPasswordField SignupPFconfPass;
    @FXML
    private Button adduserbutton;
    @FXML
    private JFXTextField SignupTXFusername;
    @FXML
    private ImageView Image;
    @FXML
    private Pane banner;
    @FXML
    private JFXDatePicker SignupDOB;
    @FXML
    private Hyperlink SignInLabel;
    @FXML
    private JFXTextField SignupTXFAdress;

    /**
     * Initializes the controller class.
     */
      @Override
      public void initialize(URL url, ResourceBundle rb) {
        addUser();
        showSignIn();
        showVerif();
        sendVerifMail();
     
    }
      
      private void addUser() {
        System.out.println(adduserbutton);
        adduserbutton.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                        java.sql.Date dateSQL = java.sql.Date.valueOf(SignupDOB.getValue());                        
                  if (SignupPFpass.getText().equalsIgnoreCase(SignupPFconfPass.getText())) {
                    if (RadioFree.isSelected()) {
                        User u = new User(SignupTXFusername.getText(), SignupPFpass.getText(), SignupTXFnom.getText(), SignupTXFprenom.getText(), RadioFree.getText(), "Inactive", SignupTXFemail.getText(), SignupTXFAdress.getText(),dateSQL);
                        ServiceUser SU = new ServiceUser();
                        SU.addUser(u);
                    } else if (RadioRecru.isSelected()) {
                        User u = new User(SignupTXFusername.getText(), SignupPFpass.getText(), SignupTXFnom.getText(), SignupTXFprenom.getText(), RadioRecru.getText(), "Inactive", SignupTXFemail.getText(), SignupTXFAdress.getText(),dateSQL);
                        ServiceUser SU = new ServiceUser();
                        SU.addUser(u);
                    } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Choississez le type de votre compte");
                    alert.setHeaderText("Choississez le type de votre compte");
                    alert.setContentText("Ooops, Choississez le type de votre compte!");
                    alert.showAndWait();
                    }
                   
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Verifier votre Mot de Passe ");
                    alert.setHeaderText("Verifier votre Mot de Passe");
                    alert.setContentText("Ooops, Verifier votre Mot de Passe    !");
                    alert.showAndWait();
                }

            }
            
        });
        

    }
    
      private void showSignIn (){
             SignInLabel.setOnAction((ActionEvent event) -> {
            try {
                Parent LoginView = FXMLLoader.load(getClass().getResource("Login.fxml"));
                Scene  LoginScene = new Scene(LoginView);
                Stage  window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(LoginScene);
                window.show();
            } catch (IOException ex){
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE,null,ex);
                } 
        });
}
            
      private void showVerif(){
            adduserbutton.setOnMouseClicked((MouseEvent event) -> {
                try {
                    Parent VerifView = FXMLLoader.load(getClass().getResource("VerifCode.fxml"));
                    Scene VerifScene = new Scene(VerifView);
                    Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    window.setScene(VerifScene);
                    window.show();
                } catch (IOException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }   
            });
    }
      
      
      private void sendVerifMail(){
          adduserbutton.setOnMouseReleased((MouseEvent event) -> {
          Mail mail = new Mail("smartstart008@gmail.com", SignupTXFemail.getText(), "Code de verification", "ggg");
          Send_mail verifMail = new Send_mail();
          verifMail.sendMail(mail);
      });
          
      }
      
}
    