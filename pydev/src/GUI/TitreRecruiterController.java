/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class TitreRecruiterController implements Initializable {

    @FXML
    private Pane banner;
    @FXML
    private Label label;
    @FXML
    private JFXTextArea titlemission;
    @FXML
    private JFXButton suivantbutton;
    @FXML
    private JFXButton Exit;
    @FXML
    private Label erreur;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setLabel(label);
       // savejobname();
      
        pagedescription();
        Exitpageprincipale();
        
    }    

    public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }
   public void verif(ActionEvent event)
   {}

   private void pagedescription(){
       suivantbutton.setOnMouseClicked((MouseEvent event) -> {
           if(titlemission.getText().length() == 0)
           {   FXMLLoader Loader = new FXMLLoader(getClass().getResource("TitreRecruiter.fxml"));
                           System.out.println(titlemission.getText());
                           erreur.setText("Merci d'Entrer le titre de la mission !");
   
           try {
                   Parent descriptionView = FXMLLoader.load(getClass().getResource("TitreRecruiter.fxml"));
                   Scene descriptionScene = new Scene(descriptionView);
                    Stage desc = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    desc.setScene(descriptionScene);
                    desc.show();
               } catch (IOException ex) {
                   Logger.getLogger(TitreRecruiterController.class.getName()).log(Level.SEVERE, null, ex);
               }
               
           
           }
           else { 
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Description.fxml"));
               Parent root = Loader.load();
               DescriptionController dc = Loader.getController();
               dc.setTitre(titlemission.getText());
               System.out.println(titlemission.getText());
              // Parent descriptionView = FXMLLoader.load(getClass().getResource("Description.fxml"));
               Scene descriptionScene = new Scene(root);
                    Stage desc = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    desc.setScene(descriptionScene);
                    desc.show();
           } catch (IOException ex) {
               Logger.getLogger(DashboardRecruiterController.class.getName()).log(Level.SEVERE, null, ex);
           }
           }      
                 });        
}
   
   private void savejobname(){
       
       suivantbutton.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
              
            }
            
            });
       
       
       
   }
   
   
   
    private void Exitpageprincipale(){
       Exit.setOnMouseClicked((MouseEvent event) -> {
           try {
                Parent titreView = FXMLLoader.load(getClass().getResource("DashboardRecruiter.fxml"));
               Scene DashScene = new Scene(titreView);
                    Stage dashrec = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    dashrec.setScene(DashScene);
                    dashrec.show();
           } catch (IOException ex) {
Logger.getLogger(DashboardRecruiterController.class.getName()).log(Level.SEVERE, null, ex);           }
                       
       });        
} 
   
   

   
  
}