/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Services.ServiceUser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import entities.Mission;
import entities.Skills;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.Statics;

/**
 * FXML Controller class
 *
 * @author achiboub
 */
public class VisibiliteController implements Initializable {
         private String titre; 
         private String details;
         private String skills;
         private String description;
         private String Visibilite;
         private String nombrefreelancers;
    @FXML
    private Pane banner;
    @FXML
    private Label label;
    @FXML
    private JFXButton Retourexpertise;
    @FXML
    private JFXButton Nextbudget;
    @FXML
    private JFXButton toutlemonde;
    @FXML
    private JFXButton seulement;
    @FXML
    private JFXButton sousinvitation;
    @FXML
    private JFXButton unseul;
    @FXML
    private JFXButton plusieurs;
    @FXML
    private JFXTextArea nombre; 
    @FXML
    private Label erreur2;
    @FXML
    private Label erreur;

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public void setDescription(String description) {
        this.description = description;
    }
         
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setLabel(label);
        retourExpertise();
        handlebuttonVisibilite();
        handlebuttonnombre();
        
        movetoBudget();
       
        // TODO
    } 
    public void setLabel(Label label) {
        this.label.setText(Statics.currentUser.getNom());
    }
     private void retourExpertise(){
       Retourexpertise.setOnMouseClicked((MouseEvent event) -> {
           try {
               
                FXMLLoader Loader = new FXMLLoader(getClass().getResource("Expertise.fxml"));
                  Parent root = Loader.load();
                  ExpertiseController ex = Loader.getController();
                  ex.setDescription(description);
                  ex.setTitre(titre);
                  ex.setDetails(details);
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene expertiseScene = new Scene(root);
                    Stage Expertise = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Expertise.setScene(expertiseScene);
                    Expertise.show();
           } catch (IOException ex) {
               Logger.getLogger(DetailsController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
       });        
}
     
       
 
 
    
  private String handlebuttonVisibilite(){
       toutlemonde.setOnMouseClicked((MouseEvent event) -> {
               Visibilite= "Tout le monde";
          toutlemonde.setOnAction((ActionEvent e) -> {
   toutlemonde.getStyleClass().add("addBobOk");
   seulement.getStyleClass().remove("addBobOk");
   sousinvitation.getStyleClass().remove("addBobOk");
});   
               
       });
       seulement.setOnMouseClicked((MouseEvent event) -> {
           
               Visibilite= "Seulement SmartStart Freelancers"; 
           seulement.setOnAction((ActionEvent e) -> {
    
    seulement.getStyleClass().add("addBobOk");
   toutlemonde.getStyleClass().remove("addBobOk");
   sousinvitation.getStyleClass().remove("addBobOk"); 
});   
                       
       });
      sousinvitation.setOnMouseClicked((MouseEvent event) -> {
           
               Visibilite= "Sous invitation seulement"; 
           sousinvitation.setOnAction((ActionEvent e) -> {
    sousinvitation.getStyleClass().add("addBobOk");
   toutlemonde.getStyleClass().remove("addBobOk");
   seulement.getStyleClass().remove("addBobOk");
});   
                       
       });
       
       
       return Visibilite;
}

  

  private String handlebuttonnombre(){
       unseul.setOnMouseClicked((MouseEvent event) -> {
               nombrefreelancers= "1";
          unseul.setOnAction((ActionEvent e) -> {
   unseul.getStyleClass().add("addBobOk");
   plusieurs.getStyleClass().remove("addBobOk");
   
});   
          
       });
       plusieurs.setOnMouseClicked((MouseEvent event) -> {
            nombrefreelancers=nombre.getText();
           plusieurs.setOnAction((ActionEvent e) -> {       
    plusieurs.getStyleClass().add("addBobOk");
   unseul.getStyleClass().remove("addBobOk");
   
});   
                       
       });
                 
     return nombrefreelancers;        
     }  
    
    
        private void movetoBudget(){
       Nextbudget.setOnMouseClicked((MouseEvent event) -> {
           if (nombrefreelancers == null) {
           erreur.setText("veuillez selectionner une option  !");
           }
           if(Visibilite == null){
          erreur2.setText("veuillez selectionner une option  !");

           }
           
           else if ((nombrefreelancers != null) && (Visibilite!= null) ){ 
           
           try {
               FXMLLoader Loader = new FXMLLoader(getClass().getResource("Budget.fxml"));
                  Parent root = Loader.load();
                  BudgetController Bu = Loader.getController();
                  Bu.setDescription(description);
                  Bu.setTitre(titre);
                  Bu.setDetails(details);
                  Bu.setSkills(skills);
                Bu.setVisibilite(Visibilite);
                Bu.setNombrefreelancers(nombrefreelancers);
                  
                  //  Parent ExpertiseView = FXMLLoader.load(getClass().getResource("Expertise.fxml"));
               Scene BudgetScene = new Scene(root);
                    Stage Expertise = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Expertise.setScene(BudgetScene);
                    Expertise.show();
           } catch (IOException ex) {
               Logger.getLogger(DetailsController.class.getName()).log(Level.SEVERE, null, ex);
           }
                       
           } });       
            }

    
    
    
    
}
