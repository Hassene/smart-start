/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

/**
 *
 * @author mahmoudh
 */
import entities.Mail;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
public class Send_mail {
    	//PARAMETER
      final String  username = "smartstart008@gmail.com";
       final String password = "smart_2019";

	

	public void sendMail(Mail mail) {
	 

       Properties prop = new Properties();
       prop.put("mail.smtp.host", "smtp.gmail.com");
       prop.put("mail.smtp.port", "587");
       prop.put("mail.smtp.auth", "true");
       prop.put("mail.smtp.starttls.enable", "true"); //TLS
       
      
       Session session = Session.getInstance(prop,
               new javax.mail.Authenticator() {
                   @Override
                   protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password);
                   }
               });

       try {

           Message message = new MimeMessage(session);
           message.setFrom(new InternetAddress(mail.getMail_from()));
           message.setRecipients(
                   Message.RecipientType.TO,
                   InternetAddress.parse(mail.getMail_to())
           );
           message.setSubject(mail.getObject());
           message.setText(mail.getMail_body());

           Transport.send(message);

           System.out.println("Le message a ete envoye avec succes");

       } catch (MessagingException e) {
           System.out.println(""+e.getMessage());
       }
   }

}
