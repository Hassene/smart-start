package entities;

public class Historique {

	private int id_historique;
	private String titre;
	private int id_mission;
	private int id_user;

	public Historique(int id_historique, String titre, int id_mission, int id_user) {
		super();
		this.id_historique = id_historique;
		this.titre = titre;
		this.id_mission = id_mission;
		this.id_user = id_user;
	}

	public int getId_historique() {
		return id_historique;
	}

	public void setId_historique(int id_historique) {
		this.id_historique = id_historique;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getId_mission() {
		return id_mission;
	}

	public void setId_mission(int id_mission) {
		this.id_mission = id_mission;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	@Override
	public String toString() {
		return "Historique [id_historique=" + id_historique + ", titre=" + titre + ", id_mission=" + id_mission
				+ ", id_user=" + id_user + "]";
	}

}
