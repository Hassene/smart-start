package entities;

public class Jobs {

	private int id_job;
	private String designation;

	public Jobs(int id_job, String designation) {
		super();
		this.id_job = id_job;
		this.designation = designation;
	}

	public int getId_job() {
		return id_job;
	}

	public void setId_job(int id_job) {
		this.id_job = id_job;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Jobs [id_job=" + id_job + ", designation=" + designation + "]";
	}

}
