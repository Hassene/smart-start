/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author mahmoudh
 */
public class Mail {
    
    private String mail_from;
    private String mail_to ;
    private String object ;
    private String mail_body;

    public Mail(String mail_from, String mail_to, String object, String mail_body) {
        this.mail_from = mail_from;
        this.mail_to = mail_to;
        this.object = object;
        this.mail_body = mail_body;
    }

    public Mail() {
    }

    @Override
    public String toString() {
        return "Mail{" + "mail_from=" + mail_from + ", mail_to=" + mail_to + ", object=" + object + ", mail_body=" + mail_body + '}';
    }

    public String getMail_from() {
        return mail_from;
    }

    public void setMail_from(String mail_from) {
        this.mail_from = mail_from;
    }

    public String getMail_to() {
        return mail_to;
    }

    public void setMail_to(String mail_to) {
        this.mail_to = mail_to;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getMail_body() {
        return mail_body;
    }

    public void setMail_body(String mail_body) {
        this.mail_body = mail_body;
    }
    
   
}
