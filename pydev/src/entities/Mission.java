package entities;

public class Mission {

	private int id_mission;
	private String titre;
        private String description;
        private String details;
        private String Skills;
        private String Visibilite;
        private String nombrefreelancers;
        private double budget;
	private int id_freelancer;
	private int id_recruiter;
	private int id_categorie;
        private String moyendepayment;
        private String niveaudexperience;
        private String duree;
        private int montantapayer;

    public void setMoyendepayment(String moyendepayment) {
        this.moyendepayment = moyendepayment;
    }

    public void setNiveaudexperience(String niveaudexperience) {
        this.niveaudexperience = niveaudexperience;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public void setMontantapayer(int montantapayer) {
        this.montantapayer = montantapayer;
    }

    

    public String getMoyendepayment() {
        return moyendepayment;
    }

    public String getNiveaudexperience() {
        return niveaudexperience;
    }

    public String getDuree() {
        return duree;
    }

    public int getMontantapayer() {
        return montantapayer;
    }

    
        

    public String getNombrefreelancers() {
        return nombrefreelancers;
    }

    public void setNombrefreelancers(String nombrefreelancers) {
        this.nombrefreelancers = nombrefreelancers;
    }

   

    public void setVisibilite(String Visibilite) {
        this.Visibilite = Visibilite;
    }

    public String getVisibilite() {
        return Visibilite;
    }

    public String getSkills() {
        return Skills;
    }

    public void setSkills(String Skills) {
        this.Skills = Skills;
    }

 
    public int getId_mission() {
		return id_mission;
	}

	public void setId_mission(int id_mission) {
		this.id_mission = id_mission;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public int getId_freelancer() {
		return id_freelancer;
	}

	public void setId_freelancer(int id_freelancer) {
		this.id_freelancer = id_freelancer;
	}

	public int getId_recruiter() {
		return id_recruiter;
	}

	public void setId_recruiter(int id_recruiter) {
		this.id_recruiter = id_recruiter;
	}

	public int getId_categorie() {
		return id_categorie;
	}

	public void setId_categorie(int id_categorie) {
		this.id_categorie = id_categorie;
	}

    @Override
    public String toString() {
        return "Mission{" + "titre=" + titre + ", description=" + description + ", details=" + details + ", Skills=" + Skills + ", Visibilite=" + Visibilite + ", nombrefreelancers=" + nombrefreelancers + ", budget=" + budget + ", id_freelancer=" + id_freelancer + ", id_recruiter=" + id_recruiter + ", id_categorie=" + id_categorie + ", moyendepayment=" + moyendepayment + ", niveaudexperience=" + niveaudexperience + ", duree=" + duree + ", montantapayer=" + montantapayer + '}';
    }

        
        
        
   


    public Mission(String titre, String description, String details, String Skills, String Visibilite, String nombrefreelancers, String moyendepayment, String niveaudexperience, String duree) {
        this.titre = titre;
        this.description = description;
        this.details = details;
        this.Skills = Skills;
        this.Visibilite = Visibilite;
        this.nombrefreelancers = nombrefreelancers;
        this.moyendepayment = moyendepayment;
        this.niveaudexperience = niveaudexperience;
        this.duree = duree;
    }

    public Mission(int id_mission, String titre, String description, String details, String Skills, String Visibilite, String nombrefreelancers, String moyendepayment, String niveaudexperience, String duree) {
        this.id_mission = id_mission;
        this.titre = titre;
        this.description = description;
        this.details = details;
        this.Skills = Skills;
        this.Visibilite = Visibilite;
        this.nombrefreelancers = nombrefreelancers;
        this.moyendepayment = moyendepayment;
        this.niveaudexperience = niveaudexperience;
        this.duree = duree;
    }

    public Mission(int id_mission, String titre) {
        this.id_mission = id_mission;
        this.titre = titre;
    }


    

 
   

 


    
    
    
    
    
    
    

}
