package entities;

public class PayPal {

	private int id_paypal;
	private int id_paiement;

	public PayPal(int id_paypal, int id_paiement) {
		super();
		this.id_paypal = id_paypal;
		this.id_paiement = id_paiement;
	}

	public int getId_paypal() {
		return id_paypal;
	}

	public void setId_paypal(int id_paypal) {
		this.id_paypal = id_paypal;
	}

	public int getId_paiement() {
		return id_paiement;
	}

	public void setId_paiement(int id_paiement) {
		this.id_paiement = id_paiement;
	}

	@Override
	public String toString() {
		return "PayPal [id_paypal=" + id_paypal + ", id_paiement=" + id_paiement + "]";
	}

}
