package entities;

public class Reclamation {

	private int id_reclamation;
	private String objet;
	private String message;
	private int id_user;

	public Reclamation(int id_reclamation, String objet, String message, int id_user) {
		super();
		this.id_reclamation = id_reclamation;
		this.objet = objet;
		this.message = message;
		this.id_user = id_user;
	}

	public int getId_reclamation() {
		return id_reclamation;
	}

	public void setId_reclamation(int id_reclamation) {
		this.id_reclamation = id_reclamation;
	}

	public String getObjet() {
		return objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	@Override
	public String toString() {
		return "Reclamation [id_reclamation=" + id_reclamation + ", objet=" + objet + ", message=" + message
				+ ", id_user=" + id_user + "]";
	}

}
