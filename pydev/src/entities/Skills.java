package entities;



public class Skills {
	private int id_skills;
	private String designation;


	public Skills(int id_skills, String designation) {
		super();
		this.id_skills = id_skills;
		this.designation = designation;
	}

	public int getId_skills() {
		return id_skills;
	}

	public void setId_skills(int id_skills) {
		this.id_skills = id_skills;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Skills [id_skills=" + id_skills + ", designation=" + designation + "]";
	}

}
